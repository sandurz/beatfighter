var app = angular.module('beatFighter', []).config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        'https://player.twitch.tv/**',
        'http://www.twitch.tv/**',
        'https://static-cdn.jtvnw.net/**'
    ])
});

app.controller('mainController', function($scope, $window, $interval) {
    scopeInit($scope,['screen','thumbnail','titleBar','player','chat']);
    $scope.screen = {};
    $scope.count = 0;
    $scope.users = [
        'beatfightercompetition',
        'hmmm23',
        'monstercat',
        'beatbreakdown',
        'actabunnifoofoo'
    ];

    var w = angular.element($window);

    $scope.setCurrentUser = function(user){
        $scope.currentStream = user;
        $scope.currentChat = user;
    }

    $scope.stepChat = function(direction){
        var length = $scope.users.length;
        var currentIndex = $scope.users.indexOf($scope.currentChat);
        var newIndex = currentIndex + direction;
        if( newIndex < 0 ){
            newIndex = newIndex + length;
        } else if ( newIndex >= length){
            newIndex = newIndex - length;
        }
        $scope.currentChat = $scope.users[newIndex];
    }
    $scope.setCurrentUser($scope.users[0]);
    $scope.sizeInit = function(){
        $scope.screen.height = $window.innerHeight;
        $scope.screen.width = $window.innerWidth;

        $scope.titleBar = 50;
        var maxName = 0;
        for(var i = 0; i < $scope.users.length; i++){
            if ($scope.users[i].length > maxName){
                maxName = $scope.users[i].length;
            }
        }
        console.log('Max characters: ',maxName);
        $scope.nameWidth = Math.floor(7.5*maxName);

        $scope.thumbnail.width = Math.floor($scope.screen.width/5);
        $scope.thumbnail.height = Math.floor((9/16) * $scope.thumbnail.width);

        $scope.player.height = $scope.screen.height - $scope.titleBar - $scope.thumbnail.height;
        $scope.player.width = Math.floor((16/9)*$scope.player.height);

        $scope.chat.height = $scope.player.height;
        $scope.chat.width = $scope.screen.width - $scope.player.width;
    }

    $scope.sizeInit();

    $scope.getEmbedUrl = function(){
        return "https://player.twitch.tv/?channel=" + $scope.currentStream;
    }

    $scope.getChatUrl = function(user){
        return "http://www.twitch.tv/" + user + "/chat";
    }

    $scope.getPreviewUrl = function(username){
        var width = $scope.thumbnail.width;
        var height = $scope.thumbnail.height;
        return "https://static-cdn.jtvnw.net/previews-ttv/live_user_" + username + "-" + width + "x" + height + ".jpg"
        + '?' + $scope.count;
    }

    $scope.addPx = function(value){
        return value + "px";
    }

    function updateCount(){
        $scope.count++;
    }

    w.bind('resize', function(){
        console.log(resize);
        $scope.sizeInit();
        scope.$digest();
    });


    $interval(updateCount,(1000*30));
});

function scopeInit(scope, fields){
    for( var x = 0; x < fields.length; x++){
        scope[fields[x]] = {};
    }
}